import { LoginService } from 'src/app/shared/service/login.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { promise } from 'protractor';
import { MesInfo } from 'src/app/feature/inscription/MesInformation';

@Injectable({
  providedIn: 'root'
})
export class LoginCanActiveService implements CanActivate {

  bool=true;

  constructor(
      private router: Router,
      private sql: LoginService,
      private info : MesInfo
  ) {
    
   }

    canActivate() : boolean | Observable<boolean> | Promise<boolean>
      {

        return this.info.getconnect().then(data=>{
        if(data){
          return data;
        }
        else{
          this.router.navigate(['/condition']);
          //console.log("****************************");
        }
        })
        
      } 
      
     getVerifie(): Promise<boolean>
      {
        return this.sql.getAllElement().then<boolean>((data) =>{
          if(data){
              if(data.rows){
                return true;
              }
          }
          else{
            return false;
          }
        }).catch(e=> {return false;});
      }
  }