import { promise } from 'protractor';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Personne } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private sqlite: SQLite) { }

  private db: SQLiteObject
   private tableLogin ='CREATE TABLE IF NOT EXISTS connection  (`username`	TEXT,password	TEXT,id	INTEGER,personne	INTEGER,PRIMARY KEY(id AUTOINCREMENT))';
   private tablePersonne='CREATE TABLE IF NOT EXISTS personne (id	INTEGER,nom	TEXT,prenom TEXT,ville	TEXT,pays	TEXT,photo	TEXT,PRIMARY KEY(id AUTOINCREMENT))';
   public createDataBase(){
      this.sqlite.create({
        name: 'dbname',
        location: 'default'
      })
      .then((db: SQLiteObject) =>{
        this.db = db;
        this.db.executeSql(this.tablePersonne, [])
      .then(() => {
        this.db.executeSql(this.tableLogin, [])
        .then(() => console.log('bas de donner creer avec susscess Executed SQL'))
        .catch(e => console.log(e));
      })
      .catch(e => console.log(e));
      })
      .catch(e=>console.log(e));
    }
  
    addUser(){
      const req= 'INSERT INTO personne (nom,prenon,ville,pays) VALUES(1, \' Richard\', \' Burkina\', \' Ouagadougou\')';
      return this.db.executeSql(req, []);
    }
  
    getAllElement(): Promise<any> {
      console.log('*************************************************');
     return  this.db.executeSql('SELECT nom FROM personne', [])
    }

    addUsert(p: Personne){
      console.log('*************************************************');
      console.log(p);
      console.log('*************************************************');
      //const req= 'INSERT INTO personne (nom,prenon,ville,pays) VALUES(1, \' Richard\', \' Burkina\', \' Ouagadougou\')';
     const req= 'INSERT INTO personne (nom,prenom,ville,photo,pays) VALUES(\''+ p.nom +'\', \''+ p.prenom+'\', \''+ p.ville+'\', \''+ p.photo+'\', \''+ p.pays+'\')';
     console.log(req); 
     return this.db.executeSql(req, []);
    }
}

