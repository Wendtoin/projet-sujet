import { TestBed } from '@angular/core/testing';

import { LoginCanActiveService } from './login-can-active.service';

describe('LoginCanActiveService', () => {
  let service: LoginCanActiveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginCanActiveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
