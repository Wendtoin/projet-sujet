import { HexBase64Latin1Encoding } from "crypto";

export class Personne {
    id: number;
    nom: string;
    prenom: string;
    ville: string;
    pays: string;
    photo: HexBase64Latin1Encoding;
}