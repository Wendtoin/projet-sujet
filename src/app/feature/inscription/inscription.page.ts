import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { LoginService } from 'src/app/shared/service/login.service';
import { ToastController } from '@ionic/angular';
import { MesInfo } from './MesInformation';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  imgTitre ='';
  liste: [];
  isActive= false;
  personneForm: FormGroup;
  constructor(private camera: Camera,private builder: FormBuilder, 
    public toastController: ToastController,private sql: LoginService, private route: Router, private info: MesInfo) { }
  ngOnInit() {
    
    this.init();
    //this.sql.createDataBase();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Bienvenu sur la PlatForm SIRA',
      duration: 2000
    });
    toast.present();
  }

  public choisirPhoto() {
    this.camera.getPicture({sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,destinationType: this.camera.DestinationType.DATA_URL})
    .then((imageData) => {
      this.imgTitre = "data:image/jpeg;base64," +imageData;
      this.personneForm.get('photo').setValue(this.imgTitre);

    }, (err) => {
     // Handle error
     console.log("Camera issue: " + err);
    });
  }

  init(){
    this.personneForm = this.builder.group({
      nom:[null, Validators.required],
      prenom: [null, Validators.required],
      ville:[null, Validators.required],
      pays:[null, Validators.required],
      photo:["./assets/logo.jpeg",],
    })
  }
  
  onSubmit(){
    this.info.saveMesInfo(this.personneForm.value)
    this.info.setisConnect();
    this.route.navigate(['/menu/dashboard']);
    this.presentToast();
    // console.log(this.personneForm.value);
    // this.sql.addUsert(this.personneForm.value)
    // .then(()=> {
    //   console.log('user ajouter');
    //   this.presentToast();
    //   this.route.navigate(['/menu/dashboard']);
    // })
    // .catch(e =>console.error(e))
  }
}
