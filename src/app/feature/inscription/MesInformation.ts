import { async } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Personne } from 'src/app/shared/models/user.model';


@Injectable({
    providedIn: 'root'
  })
export class MesInfo {
    user = 'utilisateur';
    utilisateur;
  constructor(private storage: Storage) {

  }

  setisConnect(){
      this.storage.set('1',true);
  }

   setisInit(){
     console.log()
      this.storage.set('1',false);
  }

  getconnect(): Promise<boolean>{
    console.log('hello')
      return this.storage.get('1');
  }

  saveMesInfo(users): Promise<any>{
    return this.storage.set(this.user, JSON.stringify(users));
}

   async getMesInf() : Promise<any>{  
    return this.storage.get(this.user);
}
getInfoThread(){
    return this.storage.get(this.user);
}

  /**
  ** Sauvegarde d'une note
  **/
  saveNote(note) {
    // Lorsque l'on est dans une méthode, on utilise le mot-clé "this"
    // pour pouvoir utiliser l'objet storage
    // Ici on sauvegarde une note avec comme clé son Identifiant (id)
    this.storage.set(note.id, note);
  }

  // ... Autres éléments de code

}