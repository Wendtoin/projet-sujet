import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { InscriptionPageRoutingModule } from './inscription-routing.module';
import {MatStepperModule} from '@angular/material/stepper';
import { InscriptionPage } from './inscription.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    IonicModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatInputModule,
    InscriptionPageRoutingModule,
  ],
  declarations: [InscriptionPage]
})
export class InscriptionPageModule {}
