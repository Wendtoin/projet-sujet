import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AiderPageRoutingModule } from './aider-routing.module';

import { AiderPage } from './aider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AiderPageRoutingModule
  ],
  declarations: [AiderPage]
})
export class AiderPageModule {}
