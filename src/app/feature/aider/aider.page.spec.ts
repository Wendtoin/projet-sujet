import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AiderPage } from './aider.page';

describe('AiderPage', () => {
  let component: AiderPage;
  let fixture: ComponentFixture<AiderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AiderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
