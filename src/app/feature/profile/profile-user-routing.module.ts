import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileUserPage } from './profile-user/profile-user.page';
import { UpdatePage } from './update/update.page';


const routes: Routes = [
  {
    path: '',
    component: ProfileUserPage
  },
  {
    path: 'update',
    component: UpdatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
