import { ProfilePageRoutingModule } from './profile-user-routing.module';
import { ProfileUserPage } from './profile-user/profile-user.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { UpdatePage } from './update/update.page';



@NgModule({
  declarations: [ProfileUserPage, UpdatePage],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    IonicModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatInputModule,
    ProfilePageRoutingModule
  ]
})
export class ProfileModule { }
