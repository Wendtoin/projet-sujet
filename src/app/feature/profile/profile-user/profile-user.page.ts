import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { LoginService } from 'src/app/shared/service/login.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { MesInfo } from '../../inscription/MesInformation';
import { Personne } from 'src/app/shared/models/user.model';
import { __await } from 'tslib';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.page.html',
  styleUrls: ['./profile-user.page.scss'],
})
export class ProfileUserPage implements OnInit {
  imgTitre ='';
  liste: [];
  isActive= false;
  personneForm: FormGroup;
  person: any;
  constructor(private camera: Camera,private builder: FormBuilder, 
    public toastController: ToastController,private sql: LoginService, private route: Router, private info: MesInfo) {
      //this.person=this.info.getMesInf();
      this.isActive = true;
       this.info.getMesInf().then(data=>{
        this.person = JSON.parse(data);
        console.log(this.person.nom);
        this.isActive = false;
      });
  }
  ngOnInit() {
   
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Bienvenu sur la PlatForm SIRA',
      duration: 2000
    });
    toast.present();
  }

  /*public choisirPhoto() {
    this.camera.getPicture({sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,destinationType: this.camera.DestinationType.DATA_URL})
    .then((imageData) => {
      this.imgTitre = "data:image/jpeg;base64," +imageData;
      this.personneForm.get('photo').setValue(this.imgTitre);

    }, (err) => {
     // Handle error
     console.log("Camera issue: " + err);
    });
  }*/

}
