import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { LoginService } from 'src/app/shared/service/login.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { MesInfo } from '../../inscription/MesInformation';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {
  imgTitre ='';
  liste: [];
  isActive= false;
  person : any;
  personneForm: FormGroup;
  constructor(private camera: Camera,private builder: FormBuilder, 
    public toastController: ToastController,private sql: LoginService, private route: Router, private info:MesInfo) { 

      this.isActive = true;
       this.info.getMesInf().then(data=>{
        this.person = JSON.parse(data);
        console.log(this.person.nom);
        this.personneForm = this.builder.group({
          nom:[this.person.nom, Validators.required],
          prenom: [this.person.prenom, Validators.required],
          ville:[this.person.ville, Validators.required],
          pays:[this.person.pays, Validators.required],
          photo:[this.person.photo,Validators.required],
        });
        console.log(this.personneForm.value);
        this.isActive = false;
      });
    }
  ngOnInit() {
    
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Bienvenu sur la PlatForm SIRA',
      duration: 2000
    });
    toast.present();
  }

  public choisirPhoto() {
    this.camera.getPicture({sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,destinationType: this.camera.DestinationType.DATA_URL})
    .then((imageData) => {
      this.imgTitre = "data:image/jpeg;base64," +imageData;
      this.personneForm.get('photo').setValue(this.imgTitre);

    }, (err) => {
     // Handle error
     console.log("Camera issue: " + err);
    });
  }

  init(){
    this.personneForm = this.builder.group({
      nom:[this.person.nom, Validators.required],
      prenom: [this.person.prenom, Validators.required],
      ville:[this.person.ville, Validators.required],
      pays:[this.person.pays, Validators.required],
      photo:[this.person.photo,Validators.required],
    })
  }
  onSubmit(){
    this.info.saveMesInfo(this.personneForm.value)
    this.info.setisConnect();
    this.route.navigate(['/menu/profile-user']);
    this.presentToast();
    // console.log(this.personneForm.value);
    // this.sql.addUsert(this.personneForm.value)
    // .then(()=> {
    //   console.log('user ajouter');
    //   this.presentToast();
    //   this.route.navigate(['/menu/dashboard']);
    // })
    // .catch(e =>console.error(e))
  }
}
