import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {
@ViewChild('slidesRef') slides: IonSlides;

  constructor() { }

  slideOpts = {
    initialSlide: 1,
    speed: 600,
    autoplay: true
  };

  onSlideMoved(event) {
    /** isEnd true when slides reach at end slide */
    event.target.isEnd().then(isEnd => {
    });

    event.target.isBeginning().then((istrue) => {
    });
  }

}
