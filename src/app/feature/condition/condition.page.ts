import { Component, OnInit } from '@angular/core';
import{Router } from '@angular/router'
import { UtilisationPage } from './utilisation/utilisation'
import { ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-condition',
  templateUrl: './condition.page.html',
  styleUrls: ['./condition.page.scss'],
})
export class ConditionPage implements OnInit {
  accepter = false;
  constructor(private route: Router, 
              public toastController: ToastController, 
              public modalController: ModalController) { }

  ngOnInit() {
  }
  
  verifierAccepter(){
    if(this.accepter){
      this.route.navigate(['/inscription']);
    }
    else{
      this.presentToast();
    }
    
  }

   async presentToast() {
    const toast = await this.toastController.create({
      message: 'Veuillez accepter les conditions d\'utilisation',
      duration: 2000
    });
    toast.present();
  }

  async presentModal() {
      const modal = await this.modalController.create({
        component: UtilisationPage ,
        cssClass: 'my-custom-class'
      });
      return await modal.present();
    }

}
