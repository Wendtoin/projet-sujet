import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { IonicModule } from '@ionic/angular';

import { SujetPageRoutingModule } from './sujet-routing.module';

import { SujetPage } from './sujet.page';
import { ViewSujetPage } from './view-sujet/view-sujet.page';

import { SujetServices } from './sujet-services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SujetPageRoutingModule,
    HttpClientModule
  ],
  declarations: [SujetPage,ViewSujetPage],
  exports:[ViewSujetPage],
  providers : [SujetServices]
})
export class SujetPageModule {}
