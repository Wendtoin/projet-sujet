import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { SujetList , SujetModel} from './../../shared/models/data.sujet';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SujetServices {

  constructor(private http : HttpClient) { }

  public getElement() : Observable<SujetModel[]> {
      return this.http.get<SujetModel[]>("http://153.92.208.29:9090/thoth-1.0.0-SNAPSHOT/fichier-sujet/");
  }
}

