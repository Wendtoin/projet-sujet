import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewSujetPage } from './view-sujet.page';

describe('ViewSujetPage', () => {
  let component: ViewSujetPage;
  let fixture: ComponentFixture<ViewSujetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSujetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewSujetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
