import { SujetList , SujetModel} from './../../shared/models/data.sujet';
import { HistoManager } from './../historique/histoManager';
import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { ViewSujetPage } from './view-sujet/view-sujet.page';
import { SujetServices } from './sujet-services';

@Component({
  selector: 'app-sujet',
  templateUrl: './sujet.page.html',
  styleUrls: ['./sujet.page.scss'],
})
export class SujetPage implements OnInit {
 colectSujet: SujetModel[]=[];
 listeAffiche:SujetModel[]=[];
 recherche='';
  constructor( public modalController: ModalController,
              private historique: HistoManager, 
              private sujetService: SujetServices,
              private routerOutlet: IonRouterOutlet) { }

  ngOnInit() {
    this.initializeItems();
  }
  initializeItems(){
    this.sujetService.getElement().subscribe((data)=>{
      this.colectSujet= data;
    }, (error)=>{
      this.colectSujet = SujetList;
    });
    
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.colectSujet = this.colectSujet.filter((item) => {
        return (item.appellation.toLowerCase().indexOf(val.toLowerCase()) == 0);
      })
    }
  }

  rechercher()
  {
    this.listeAffiche = [];
    this.colectSujet.forEach((element, index)=>{
        if(element.appellation.toLowerCase() == this.recherche.toLowerCase()){
          this.listeAffiche.push(element);
        }
    })
  }

  async presentModal(su) {
    this.historique.addHistorique(su);
    const modal = await this.modalController.create({
      component: ViewSujetPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        'sujet': su
      }
    });
    return await modal.present();
  }

}
