import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SujetPage } from './sujet.page';

describe('SujetPage', () => {
  let component: SujetPage;
  let fixture: ComponentFixture<SujetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SujetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SujetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
