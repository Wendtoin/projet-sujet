import { async } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Personne } from 'src/app/shared/models/user.model';
import { ThrowStmt } from '@angular/compiler';
import { SujetModel } from 'src/app/shared/models/data.sujet';

export interface ModelHistorique{
    date: Date,
    sujet : SujetModel
}

@Injectable({
    providedIn: 'root'
  })
export class HistoManager {
    user = 'utilisateur';
    utilisateur;
    his='historique'
  constructor(private storage: Storage) {

  }
  addHistorique(Model: SujetModel){
    var h:ModelHistorique = {date: new Date(), sujet: Model};
    console.log(h);
    this.storage.get(this.his).then((items: ModelHistorique[])=>
      {
          if(items)
          {
            console.log("******************");
            var list:ModelHistorique[] = [h];
            Array.prototype.push.apply(list, items);
            this.storage.set(this.his,list);
            console.log("******************");
          }
          else{
            console.log("*********5555*********");
               this.storage.set(this.his, [h]);
               console.log("**********444********");
          }
      })
  }

  getAll(): Promise<ModelHistorique[]>{
      return this.storage.get(this.his);
  }

  setisConnect(){
      this.storage.set('1',true);
  }

  getconnect(): Promise<boolean>{
      return this.storage.get('1');
  }

  saveMesInfo(users): Promise<any>{
    return this.storage.set(this.user, JSON.stringify(users));
}

   async getMesInf() : Promise<any>{  
    return this.storage.get(this.user);
}
getInfoThread(){
    return this.storage.get(this.user);
}

  /**
  ** Sauvegarde d'une note
  **/
  saveNote(note) {
    // Lorsque l'on est dans une méthode, on utilise le mot-clé "this"
    // pour pouvoir utiliser l'objet storage
    // Ici on sauvegarde une note avec comme clé son Identifiant (id)
    this.storage.set(note.id, note);
  }

  // ... Autres éléments de code

}