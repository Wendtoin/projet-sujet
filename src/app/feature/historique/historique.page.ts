import { HistoManager, ModelHistorique } from './histoManager';
import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { ViewSujetPage } from '../sujet/view-sujet/view-sujet.page';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.page.html',
  styleUrls: ['./historique.page.scss'],
})
export class HistoriquePage implements OnInit {

  isActive = false;
  list: ModelHistorique[];
  constructor(private historique: HistoManager,public modalController: ModalController, private routerOutlet: IonRouterOutlet) {
      historique.getAll().then(data=>{
        this.list = data;
        console.log(data);
      });
   }

  ngOnInit() {
  }

  async presentModal(su) {
    const modal = await this.modalController.create({
      component: ViewSujetPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        'sujet': su
      }
    });
    return await modal.present();
  }

}
