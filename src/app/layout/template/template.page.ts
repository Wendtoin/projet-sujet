import { MenuController, PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { MenuHeaderPage } from '../menu-header/menu-header.page';

@Component({
  selector: 'app-template',
  templateUrl: './template.page.html',
  styleUrls: ['./template.page.scss'],
})
export class TemplatePage implements OnInit {

  titre : string;
  constructor(private menuContoller: MenuController, public popoverController: PopoverController) { }

  ngOnInit() {
  }

  public titreToolbar($even){
    this.titre = $even;
  }

  public openMenu(){
    this.menuContoller.open('main-menu');
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: MenuHeaderPage,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

}
