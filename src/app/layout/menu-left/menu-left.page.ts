import { Router } from '@angular/router';
import { menu, Menus } from './../menu';
import { Component, OnInit, Output, ViewChildren } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { EventEmitter } from '@angular/core';
import { LoginService } from 'src/app/shared/service/login.service';
import { Personne } from 'src/app/shared/models/user.model';
import { MesInfo } from 'src/app/feature/inscription/MesInformation';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.page.html',
  styleUrls: ['./menu-left.page.scss'],
})
export class MenuLeftPage implements OnInit {

  menu:menu[] = Menus;
  user: Personne;
  isLord=false;
  @Output() titreEvent = new EventEmitter<string>();
  constructor(private route: Router, private menuAffiche: MenuController,private sql: MesInfo) { }

  ngOnInit() {
    this.isLord = true;
    this.sql.getMesInf().then(data=>{
      this.user = JSON.parse(data);
      this.sql.utilisateur= this.user;
      this.isLord = false;
    })
    //this.getUser();
  }

  public change(m: menu){
    this.menuAffiche.close("main-menu");
    this.titreEvent.emit(m.label);
    this.route.navigate([m.route]);
  }


}
