import { TemplatePage } from './template/template.page';
import { MenuHeaderPage } from './menu-header/menu-header.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MenuLeftPage } from './menu-left/menu-left.page';
import { LayoutRoutingModule } from './layout-routing';
import {MatMenuModule} from '@angular/material/menu';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LayoutRoutingModule,
    MatMenuModule
  ],
  declarations: [MenuLeftPage, MenuHeaderPage, TemplatePage],
  exports:[TemplatePage]
})
export class LayoutModule { }
