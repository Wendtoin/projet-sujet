import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.page.html',
  styleUrls: ['./menu-header.page.scss'],
})
export class MenuHeaderPage implements OnInit {

  @Input('titre')
  titre;
  constructor() { }

  ngOnInit() {
  }

}
