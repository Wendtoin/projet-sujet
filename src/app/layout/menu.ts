

export class menu {
    label: string;
    icon: string;
    route: string;
    id: number;
}


export const Menus: menu[] = [
    {
        label: "Rechercher des corrections",
        icon: "book",
        route: "/menu/dashboard",
        id: 1,
    },
    {
        label: "Historiques",
        icon: "archive-outline",
        route: "/menu/historique",
        id: 2,
    },
    {
        label: "En savoir plus",
        icon: "home",
        route: "/menu/sujet",
        id: 3,
    }
]