import { TemplatePage } from './layout/template/template.page';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginCanActiveService } from './shared/service/login-can-active.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/sujet',
    pathMatch: 'full'
  },
  {
    path: 'menu',
    canActivate: [LoginCanActiveService],
    component: TemplatePage,
    children:[{
      path: 'dashboard',
      loadChildren: () => import('./feature/sujet/sujet.module').then( m => m.SujetPageModule)
    },
    {
      path: 'aider',
      loadChildren: () => import('./feature/aider/aider.module').then( m => m.AiderPageModule)
    },
    
  {
    path: 'historique',
    loadChildren: () => import('./feature/historique/historique.module').then( m => m.HistoriquePageModule)
  },

    {
      path: 'sujet',
      loadChildren: () => import('./feature/dashboard/dashboard.module').then( m => m.DashboardPageModule)
      
    },
    {
      path: 'profile-user',
      loadChildren: () => import('./feature/profile/profile.module').then( m => m.ProfileModule)
    },]
  },
  {
    path: 'inscription',
    loadChildren: () => import('./feature/inscription/inscription.module').then( m => m.InscriptionPageModule)
  },
  {
    path: 'condition',
    loadChildren: () => import('./feature/condition/condition.module').then( m => m.ConditionPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
