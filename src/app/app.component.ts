import { Router } from '@angular/router';
import { LoginService } from './shared/service/login.service';
import { AfterContentInit, AfterViewInit, Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MesInfo } from 'src/app/feature/inscription/MesInformation';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  isActive= false;
  i=0;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userExiste: LoginService,
    private route: Router,
    private mesInfo : MesInfo,
  ) {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      if(this.mesInfo.getconnect()){

      }else{
        this.mesInfo.setisInit();
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  userDesigne(){
  }
}
